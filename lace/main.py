import numpy as np
import math
import matplotlib.pyplot as plt
import argparse

def plot_results(pos: np.array, vel: np.array) -> None:
    ig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10, 4))

    # Plot the data on the subplots
    ax1.plot(pos[0,:], pos[1,:])
    ax1.set_title('position')
    ax1.set_xlabel('x')
    ax1.set_ylabel('y')

    ax2.plot(vel[0,:])
    ax2.plot(vel[1, :])
    ax2.set_title('Velocities')
    ax2.set_xlabel('steps')
    ax2.set_ylabel('vel_y')

    # Display the plots
    plt.show()

    return

def sim():
    final_time = 3
    num_time_steps = 100
    time_increment = final_time / num_time_steps
    time = np.linspace(0, final_time, num_time_steps)

    possition = np.ones((2, num_time_steps))
    velocity = np.ones((2, num_time_steps))
    acceleration = np.array([0, -9.8])

    possition[:, 0] = 0
    initial_velocity = 20
    velocity[0, 0] = initial_velocity * np.cos(np.deg2rad(45))
    velocity[1, 0] = initial_velocity * np.sin(np.deg2rad(45))

    i = 0
    while i < num_time_steps - 1:
        possition[:, (i + 1)] = possition[:, i] + (acceleration * time_increment + velocity[:, i]) * time_increment
        velocity[:, (i + 1)] = velocity[:, i] + acceleration * time_increment
        i = i + 1

    return possition, velocity
def sim_with_friction():
    final_time = 3
    num_time_steps = 100
    time_increment = final_time / num_time_steps
    time = np.linspace(0, final_time, num_time_steps)

    possition = np.ones((2, num_time_steps))
    velocity = np.ones((2, num_time_steps))
    acceleration = np.array([0, -9.8])

    possition[:, 0] = 0
    initial_velocity = 20
    velocity[0, 0] = initial_velocity * np.cos(np.deg2rad(45))
    velocity[1, 0] = initial_velocity * np.sin(np.deg2rad(45))

    mass = 1
    friction_force = np.array([0, 100])
    friction_acceleration = friction_force / mass

    i = 0
    while i < num_time_steps - 1:
        possition[:, (i + 1)] = possition[:, i] + (acceleration * time_increment + velocity[:, i]) * time_increment + \
                                velocity[:, i] / np.abs(velocity[:, i]) * (friction_acceleration * time_increment) * time_increment
        velocity[:, (i + 1)] = velocity[:, i] + acceleration * time_increment

        i = i + 1

    return possition, velocity

if __name__ == '__main__':
    possition, velocity = sim()
    plot_results(possition, velocity)

    possition, velocity = sim_with_friction()
    plot_results(possition, velocity)


