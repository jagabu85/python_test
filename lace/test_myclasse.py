import unittest
from myclasse import MyClasse

class TestRobot(unittest.TestCase):
    #python3 -m unittest myclasse.py
    def test_is_in_tabletop(self) -> None:
        """ Check lower outside, in adn upper ouside of the table in both
        directions

        :return: None
        """
        expected_results = 10.1
        foo = MyClasse([1, 2, 3, 4, 5])
        foo[1] = expected_results
        foo[1]
        self.assertEqual(foo[1], expected_results,
                         f'The result should be {expected_results}')

if __name__ == '__main__':
    unittest.main()
