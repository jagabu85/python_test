# Conda
```
conda update conda - Update your conda installation:
conda install -c anaconda certifi - Update the SSL certificates
conda config --set ssl_verify False - Set the SSL certificate verification to false
conda create -n myenv
conda activate myenv
conda deactivate
```
# Python Script set up Windos Power Shell
- Install python in windows (make sure to add the path)
- Check the version & path: 
```
python --version
Get-Command python
```

# Poetry
https://www.youtube.com/watch?v=Qks3eqlImy8
```
pip install poetry
poetry new name_project
pip freeze (print packages I have)
cd name_project
where python
poetry env use C:\Users\galvezbu\AppData\Local\Programs\Python\Python311\python.exe
```
It return the Using virtualenv: C:\Users\galvezbu\AppData\Local\pypoetry\Cache\virtualenvs\poetry-lace-23jb05sn-py3.11
Then we should activate it
```
C:\Users\galvezbu\AppData\Local\pypoetry\Cache\virtualenvs\poetry-lace-23jb05sn-py3.11\Scripts\activate
```

