import numpy as np
import math
import matplotlib.pyplot as plt


def operations:
    function_list = '''
    - Basic Mathematical Functions:
      - np.add()
      - np.subtract()
      - np.multiply()
      - np.divide()
      - np.power()
      - np.sqrt()
      - np.exp()
      - np.log()
      - np.log10()
      - np.abs()
      - np.sin(), np.cos(), np.tan()
      - np.arcsin(), np.arccos(), np.arctan()
      - np.deg2rad()
      - np.rad2deg()

    - Aggregation Functions:
      - np.sum()
      - np.mean()
      - np.median()
      - np.min(), np.max()
      - np.std()
      - np.var()

    - Linear Algebra Functions:
      - np.dot()
      - np.linalg.inv()
      - np.linalg.det()
      - np.linalg.eig()

    - Random Number Generation Functions:
      - np.random.rand()
      - np.random.randn()
      - np.random.randint()
    '''
    print(function_list)

def numpy_basics_1():
    print_split_start(numpy_basics_1.__name__, 80)

    print(f'np.zeros(12).reshape(3, 4): \n{np.zeros(12).reshape(3, 4)}')
    a = np.array([1, 2, 3, 2, 3, 4, 3, 4, 5, 6])
    b = np.array([7, 2, 8, 2, 7, 4, 9, 4, 9, 8])
    print(f'a: {a}')
    print(f'b: {b}')
    print(f'np.intersect1d(a,b): {np.intersect1d(a, b)}')
    print(f'np.setdiff1d(a, b): {np.setdiff1d(a, b)}')
    print(f'np.where(a == b): {np.where(a == b)}, where type is: {type(np.where(a == b))}')
    c = np.array(np.where(a == b))
    print(f'c = type(np.where(a == b)): {c}')
    for i in c[0]:
        print(f'i: {i}')
    print(f'a[(a >= 5) & (a <= 10)]: {a[(a >= 5) & (a <= 10)]}')

    print_split_end(80)

    return


def print_split_start(title: str, total_num_hash: int):
    if len(title)&1 == 0:
        num_hash = math.ceil(total_num_hash*0.5) - int(len(title) * 0.5) - 1
        print(f'\n{"#"*num_hash} {title} {"#"*num_hash}')
    else:
        num_hash = math.ceil(total_num_hash*0.5) - int(len(title) * 0.5) - 1
        print(f'\n{"#" * num_hash} {title} {"#" * (num_hash - 1)}')
    return


def print_split_end(total_num_hash: int):
    if total_num_hash & 1 == 0:
        print(f'{"#" * total_num_hash}\n')
    else:
        print(f'{"#" * total_num_hash + 1}\n')
    return


def print_small_split(total_num_hash: int):
    print(f'{"-" * total_num_hash}')
    return


def pair_max(x: int, y: int):
    # using lambda
    # lambda returns a tuple
    # map applies lambda fuction to each pair in x,y and returns an iterator
    maximum = [max(a,b) for a,b in map(lambda a,b:(a,b),x,y)]

    # using zip
    maximum = []
    for each_pair in zip(x, y):
        maximum.append(max(each_pair))

    # using zip one line
    maximum = np.array([max(each_pair) for each_pair in zip(x, y)])

    return np.array(maximum)


def lambda_functions_and_map():
    print_split_start(lambda_functions_and_map.__name__, 80)

    # map() applies the lambda function to each element of numbers, it returns an iterator which is converted in a list.
    numbers = [1, 2, 3, 4, 5]
    squared_numbers = list(map(lambda x: x ** 2, numbers))
    print(f'squared_numbers: {squared_numbers}')
    print(f'squared_numbers = list(map(lambda x: x ** 2, numbers)) {squared_numbers}')

    a = np.array([5, 7, 9, 8, 6, 4, 5])
    b = np.array([6, 3, 4, 8, 9, 7, 1])
    print(f'a: {a}')
    print(f'a: {b}')
    print(f'pair_max(a, b): {pair_max(a, b)}')

    print_split_end(80)

    return

def numpy_basics_2():
    print_split_start(numpy_basics_2.__name__, 80)
    arr = np.arange(9).reshape(3, 3)
    for a in arr:
        print(f'a:: {a}')

    print(f'Original array:\n {arr}')
    print(f'column: {arr[:,2]}')
    print(f'Modified array columns:\n {arr[:, [1, 2, 1]]}') # Selects the columsn 1, 2, 1
    print(f'Modified array rows:\n {arr[[1, 0, 1], :]}') # Selects the rows 1, 0, 1
    print(f'Reverse the rows:\n {arr[::-1, :]}')
    print(f'Reverse the columns:\n {arr[:, ::-1]}')

    rand_arr = np.random.uniform(5, 10, size=(5, 3))
    np.set_printoptions(precision=3) # only prints 3 decimals
    print(f'np.random.uniform(5, 10, size=(5, 3)): \n {rand_arr}')

    np.random.seed(100) # set the seed for random generation numbers
    rand_arr = np.random.random([3, 3]) / 1e3 # np.random.random makes beteween 0 and 1
    print(f'rand_arr = np.random.random([3, 3]) / 1e3: \n {rand_arr}')
    np.set_printoptions(suppress=False)
    print(f'np.set_printoptions(suppress=False), rand_arr:\n {rand_arr}')
    np.set_printoptions(suppress=True)
    print(f'np.set_printoptions(suppress=True), rand_arr:\n {rand_arr}')
    np.set_printoptions(suppress=False)

    a = np.arange(15)
    np.set_printoptions(threshold=6) # Define number of elements to print
    print(f'np.set_printoptions(threshold=6), a:\n {a}')

    a = np.random.random([1, 30]) / 1e3 # np.random.random makes beteween 0 and 1
    print(f'a: \n{a}')
    a_normalized = a/(max(a[0]))
    print(f'a normalized: \n {a_normalized}')
    #plt.plot(a_normalized[0])
    #plt.show()

    a_normalized = np.append(a_normalized, np.nan)
    print(f'np.isnan(a_normalized).any()= {np.isnan(a_normalized).any()}')
    a_normalized[np.isnan(a_normalized)] = 0
    print(f'np.isnan(a_normalized):\n {np.isnan(a_normalized)}')
    print(f'np.isnan(a_normalized).any()= {np.isnan(a_normalized).any()}')

    np.set_printoptions(threshold=10)
    a = np.array([1, 2, 7, 4, 5, 6, 76, 7, 7])
    print(f'a:\n {a}')
    # np.unique returns a tuple of np.arrays.
    # 1st: array of unique elements,
    # 2nd: array with positions of the unique element
    print(f'len(np.unique(a, return_counts=7)): {len(np.unique(a, return_counts=7))}')
    print(f'type(np.unique(a, return_counts=7)): {type(np.unique(a, return_counts=7))}')
    print(f'np.unique(a, return_counts=7):\n {np.unique(a, return_counts=7)}')

    np.random.seed(41)
    arr = np.random.uniform(0, 5, size=10)
    print(f'arr:\n {arr}')
    bins = np.array([0, 3, 5, 7])
    inds = np.digitize(arr.astype('float'), bins)
    print(f'inds:\n {inds}')
    labels = {1: 'small', 2: 'medium', 3: 'large'}
    print(f'type(labels): {type(labels)}')
    groups = [labels[x] for x in inds]
    print(f'groups:\n {groups}')

    print_split_end(80)
    return
def numpy_basics_3():
    print_split_start(numpy_basics_3.__name__, 80)

    # Join arrays hstack
    a = np.array((1, 2, 3)) # Tuple or list as np.array argument
    b = np.array((4, 5, 6))
    print(f'a:\n {a}')
    print(f'b:\n {b}')
    print(f'np.hstack((a, b)):\n {np.hstack((a, b))}')
    a = np.array([[1], [2], [3]])
    b = np.array([[4], [5], [6]])
    print(f'a:\n {a}')
    print(f'b:\n {b}')
    print(f'np.hstack((a, b)):\n {np.hstack((a, b))}')
    print_small_split(80)

    arr = np.random.uniform(0, 5, size=25).reshape(5,5)
    print(f'arr:\n {arr}')
    col_1 = arr[:, 0].astype('float')
    col_2 = arr[:, 2].astype('float')
    print(f'len(col_1): {len(col_1)}')
    print(f'np.shape(col_1): {np.shape(col_1)}')
    print(f'np.size(arr): {np.size(arr)}')
    print(f'np.shape(arr): {np.shape(arr)}')
    new_column = col_1 * col_2 * np.pi
    print(f'new_colum: {new_column}')
    print(f'np.shape(new_column): {np.shape(new_column)}')
    new_column = new_column[:, np.newaxis]
    print(f'np.shape(new_column): {np.shape(new_column)}')
    print(f'Added column:\n {np.hstack([arr, new_column])}')
    print_small_split(80)

    np.random.seed(100)
    a = np.array(['Iris-setosa', 'Iris-versicolor', 'Iris-virginica'])
    species_out = np.random.choice(a, 10, p=[0.5, 0.25, 0.25])
    print(f'species_out:\n {species_out}')
    print(np.unique(species_out, return_counts=True))
    print_small_split(80)

    arr = np.random.uniform(0, 5, size=10)
    print(f'arr:\n {arr}')
    print(f'second largest value of an array  arr:\n {np.unique(np.sort(arr))[-2]}')
    print_small_split(80)

    arr1 = np.arange(3)
    print(f'arr1 = np.arange(3) \n {arr1}')
    arr2 = np.arange(3, 7)
    print(f'arr2 = np.arange(3, 7) \n {arr2}')
    arr3 = np.arange(7, 10)

    arr_2d = np.concatenate([arr1, arr2, arr3])
    print(f'arr_2d = np.concatenate([arr1, arr2, arr3]): \n {arr_2d = np.concatenate([arr1, arr2, arr3])}')




    print_split_end(80)
    return


if __name__ == '__main__':
    # https://www.kaggle.com/code/themlphdstudent/learn-numpy-numpy-50-exercises-and-solution/notebook
    #numpy_basics_1()
    #lambda_functions_and_map()
    #numpy_basics_2()
    numpy_basics_3()


