import numpy as np
import math
import matplotlib.pyplot as plt
import argparse

if __name__ == '__main__':
    file_name = 'info.txt'
    try:
        with open(file_name) as file:
            print(f'first 3 characters: {file.read(3)}')
            print(f'read line: {file.readline()}')
            print(f'full file: \n {file.read()}')

        print('----------------------------')
        with open(file_name) as file:
            line = file.readline()
            while line:
                print(line, end='')
                line = file.readline()

        print('\n ----------------------------')
        with open(file_name) as file:
            lines = file.readlines()
        print(f'type(lines): {type(lines)}')
        print(f'lines: {lines}')

    except FileNotFoundError:
        print(f"Error: The file '{file_name}' was not found.")

    except IOError:
        print(f"Error: An I/O error occurred while processing the file '{file_name}'.")

    example_dict = {
        "key1": 1,
        "key2": "value2",
        "key3": "value3"
    }

    print(f'example dictionary: {example_dict["key1"]}')
