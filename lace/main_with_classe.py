import numpy as np
import math
import matplotlib.pyplot as plt
from myclasse import MyClasse



if __name__ == '__main__':
    # Create an instance of the class
    object = MyClasse([1, 2, 3, 4, 5])

    # Examples of using the special methods
    print(object)
    print(len(object))
    print(object[2])
    object[1] = 10.1
    print(object[1])
    object(1, 2, parametro="valor")
    print(f'Example static method', MyClasse.multiply_numbers(2,3))

