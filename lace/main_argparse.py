import numpy as np
import math
import matplotlib.pyplot as plt
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('arg_1')
    parser.add_argument('arg_2')
    args = parser.parse_args()

    print(f'Argument 1: {args.arg_1}, argument 2: {args.arg_2}')

