class MyClasse:
    def __init__(self, valor):
        self.foo = valor

    def __str__(self):
        return f"MyClasse({self.foo})"

    def __len__(self):
        return len(self.foo)

    def __getitem__(self, index):
        return self.foo[index]

    def __setitem__(self, index: int, value: float):
        """
        Set the value

        :param index: int number use as index for foo variable
        :param value: float value to be stored
        :return:
        """

        self.foo[index] = value

    def __call__(self, *args, **kwargs):
        print(f"Callin MyClasse with argument: {args}, {kwargs}")

    @staticmethod
    def multiply_numbers(a:int, b:int) -> int:
        return a * b