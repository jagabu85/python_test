import numpy as np
import logging

from table import Table

logging.basicConfig(filename='robot.log', filemode='w',
                    format='%(asctime)s - %(name)s - %(levelname)s - %('
                           'message)s')


class Robot:
    """
    The robot class represent the position in a confined positive 2D space
    of an object which has orientation. Also it contains the movement/actions
    functions.
    """
    possible_directions = ('NORTH', 'EAST', 'SOUTH', 'WEST')
    orders = ('PLACE', 'MOVE', 'LEFT', 'RIGHT', 'REPORT')

    def __init__(self, tabletop: Table):
        self.current_position = None
        self.orientation = None
        self.file = 'output.txt'
        self.__create_log_file()
        self.tabletop = tabletop

    def __create_log_file(self) -> None:
        """ Initialize the log file, it is reset each time

        :return: None
        """
        with open(self.file, 'w'):
            pass

    # -------------------------------------------------------------------------
    def rotation_left(self) -> None:
        """ Change the orientation of the robot 90 degree in counterclock
        direction

        :return: None
        """
        if self.is_robot_placed():
            index = self.possible_directions.index(self.orientation)

            if index == 0:
                self.orientation = (self.possible_directions[
                    len(self.possible_directions) - 1])
            else:
                self.orientation = self.possible_directions[index - 1]
        else:
            logging.error('robot.py/rotation_left: Robot should be placed '
                          'before to rotate')

    def rotation_rigth(self) -> None:
        """ Change the orientation of the robot 90 degree in clock direction

        :return: None
        """
        if self.is_robot_placed():
            index = self.possible_directions.index(self.orientation)

            if not index == len(self.possible_directions) - 1:
                self.orientation = self.possible_directions[index + 1]
            else:
                self.orientation = self.possible_directions[0]
        else:
            logging.error('robot.py/rotation_rigth: Robot should be placed '
                          'before to rotate')

    def move(self) -> None:
        """ Move the robot one unit table in the direction it is oriented

        :return: None
        """
        movements_logic = {'NORTH': (0, 1),
                           'SOUTH': (0, -1),
                           'EAST': (1, 0),
                           'WEST': (-1, 0)}
        if self.is_robot_placed():
            try_position = (self.current_position +
                            movements_logic[self.orientation])
        if self.is_robot_placed() and self.tabletop.is_in_tabletop(
                try_position):
            self.current_position = try_position
        else:
            logging.error(
                'robot.py/move: The robot has not been placed or the '
                'next position would be outside the tabletop')

    # -------------------------------------------------------------------------
    def place_robot(self, x: int, y: int, orientation: str) -> None:
        """ Define the initial position and orientation of the robot

        :param [int] x: Coordinate in X
        :param [int] y: Coordinate in Y
        :param [str] orientation: Orientation of the robot
        :return: None
        """
        self.set_current_position(x, y)
        self.orientation = orientation

    def set_current_position(self, x: int, y: int) -> None:
        """ Set the tuple current position with the coordinates X and Y

        :param [int] x: Coordinate in X
        :param [int] y: Coordinate in Y
        :return: None
        """

        if self.tabletop.is_in_tabletop(np.array([x, y])):
            self.current_position = np.array([x, y])

    def set_orientation(self, orientation):
        if self.is_valid_orientation(orientation):
            self.orientation = orientation
        else:
            logging.error('robot.py/set_orientation it can only be set with '
                          'the the orientations defined in '
                          'self._possible_directions')

    def is_robot_placed(self) -> bool:
        """ Check if the robot has alrady been placed in the table.

        :return: True if it has already been placed, false otherwise
        :rtype bool
        """
        if self.current_position is not None:
            return True
        else:
            return False

    def is_valid_orientation(self, orientation: str) -> bool:
        """ Check if the orientation is one of the available directions

        :param [str] orientation: Direction of the robot
        :return: bool
        """
        if orientation in self.possible_directions:
            return True
        else:
            return False

    # -------------------------------------------------------------------------
    def report(self) -> None:
        """ Save the current configuration of the robot to a file called
        self.fiel,if it has not been placed this command is ignored.

        :return: None
        """
        if self.is_robot_placed():
            with open(self.file, 'a') as file:
                output = (f"{self.current_position[0]},"
                          f"{self.current_position[1]},"
                          f"{self.orientation}\n")
                file.write(output)
        else:
            with open(self.file, 'a') as file:
                file.write('not in place')
