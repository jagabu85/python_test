from robot import Robot
from cli import CLI
from utils import read_from_file

from typing import Callable


class Simulate:

    def __init__(self, cli: CLI, robot: Robot):
        self.cli = cli
        self.test_robot = robot
        self.robot_orders: dict[str, Callable] = {
            "MOVE": self.test_robot.move,
            "LEFT": self.test_robot.rotation_left,
            "RIGHT": self.test_robot.rotation_rigth,
            "REPORT": self.test_robot.report
        }

    def simulate (self) -> None:
        """ Get the impute method from prompt and launch the simulation
        according to it

        :return: None
        """
        input_method: dict[str, Callable] = {
            "FILE": self.simulate_with_file,
            "CLI": self.simulate_with_CLI
        }
        order = self.cli.read_input_method()
        input_method[order]()

    # -------------------------------------------------------------------------
    def simulate_with_file(self):
        """Launch the simulation and executes the order comming from a file

        :return: None
        """
        self.cli.print_initial_message('automatically reading from a file',
                                       self.test_robot.orders)
        read_from_file() # TODO


    def simulate_with_CLI(self):
        """Launch the simulation and executes the order comming from the CLI

        :return: None
        """
        self.cli.print_initial_message('manually',
                                       self.test_robot.orders)
        configuration = self._get_initial_place_configuration()
        self._execute_place_robot(configuration)
        self._execute_orders_from_cli()
        self.cli.print_ending_message()

    # -------------------------------------------------------------------------
    def _get_initial_place_configuration(self) -> None:
        """ Read the prompt until it gets a well defined PLACE order and
        return the arguments in a dictionary

        :return [dict] configuration:  dictionary containing the
        arguments:  X, Y and orientation
        :rtype dict
        """
        configuration = self.cli.read_init_place_order(self.test_robot)
        return configuration

    def _execute_place_robot(self, configuration: dict) -> None:
        """ Execute place order for the robot using the configuration pased
        as argument

        :param [dict] configuration: dictionary containing the
        arguments:  X, Y and orientation
        :return: None
        """
        self.test_robot.place_robot(configuration['x'],
                                    configuration['y'],
                                    configuration['orientation'])

    def _execute_orders_from_cli(self) -> None:
        """ Read constantlly the prompt and execute the orders

        :return: None
        """
        order = self.cli.read_order(self.test_robot.orders)
        while order != 'EXIT':
            if order[:5] == 'PLACE' and self.cli.check_place_order(order,
                                                            self.test_robot):
                configuration = self.cli.get_configuration_from_place_order(
                    order)
                #self.test_robot.check_place_config
                self.test_robot.place_robot(configuration['x'],
                                            configuration['y'],
                                            configuration['orientation'])
            elif order[:5] != 'PLACE' and order is not '':
                self.robot_orders[order]()
            order = self.cli.read_order(self.test_robot.orders)