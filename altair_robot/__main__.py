from simulate import Simulate
from robot import Robot
from cli import CLI
from table import Table


def main():
    tabletop = Table((5, 5))
    cli = CLI(tabletop)
    robot = Robot(tabletop)
    simulation = Simulate(cli, robot)
    simulation.simulate()


if __name__ == "__main__":
    main()