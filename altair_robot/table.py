import numpy as np


class Table:
    """
    This class represents a square tabletop
    """

    def __init__(self, tabletop_size):
        self._x_domain = (0, tabletop_size[0])
        self._y_domain = (0, tabletop_size[1])

    def is_in_tabletop(self, position: np.ndarray) -> bool:
        """ Check if the position is inside the limits of the tabletop

        :param [np.array] position: array with X and Y coordinates of the
        robot
        :return: True if it is inside, false otherwise
        :rtype bool
        """
        if (self.is_in_the_domain(position[0], self._x_domain) and
                self.is_in_the_domain(position[1], self._y_domain)):
            return True
        else:
            return False


    @staticmethod
    def is_in_the_domain(value: int, domain: tuple) -> bool:
        """ Check if a value is between two values

        :param [int] value: Value to be tested
        :param [tuple] domain: Limints in which the value should be
        :return: bool
        """
        if domain[0] <= int(value) < domain[1]:
            return True
        else:
            return False