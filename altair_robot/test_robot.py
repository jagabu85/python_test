import unittest

from robot import Robot
from table import Table

class TestRobot(unittest.TestCase):

    def test_general(self) -> None:
        """ General case defined for the developer.
        Test all the tabletop limits, and check the 5 main functions place,
        left, right, move and report

        :return: None
        """
        tabletop = Table((5, 5))
        test_robot = Robot(tabletop)
        test_robot.place_robot(1, 1, 'EAST')
        test_robot.rotation_left()
        test_robot.report()
        test_robot.rotation_left()
        test_robot.report()
        test_robot.rotation_left()
        test_robot.report()
        test_robot.rotation_left()
        test_robot.report()
        test_robot.rotation_rigth()
        test_robot.report()
        test_robot.rotation_rigth()
        test_robot.report()
        test_robot.rotation_rigth()
        test_robot.report()
        test_robot.rotation_rigth()
        test_robot.report()
        test_robot.move()
        test_robot.move()
        test_robot.move()
        test_robot.move()
        test_robot.move()
        test_robot.move()
        test_robot.report()
        test_robot.rotation_left()
        test_robot.move()
        test_robot.move()
        test_robot.move()
        test_robot.move()
        test_robot.move()
        test_robot.move()
        test_robot.report()
        test_robot.rotation_left()
        test_robot.move()
        test_robot.move()
        test_robot.move()
        test_robot.move()
        test_robot.move()
        test_robot.move()
        test_robot.report()
        test_robot.rotation_left()
        test_robot.move()
        test_robot.move()
        test_robot.move()
        test_robot.move()
        test_robot.move()
        test_robot.move()
        test_robot.report()

        benchmark = ['1,1,NORTH\n',
                     '1,1,WEST\n',
                     '1,1,SOUTH\n',
                     '1,1,EAST\n',
                     '1,1,SOUTH\n',
                     '1,1,WEST\n',
                     '1,1,NORTH\n',
                     '1,1,EAST\n',
                     '4,1,EAST\n',
                     '4,4,NORTH\n',
                     '0,4,WEST\n',
                     '0,0,SOUTH\n']

        self.check_results(benchmark, test_robot)


    def test_altair_1(self) -> None:
        """ Case 1 coming from Altair:
        check ignores commands before robot is put into place

        :return: None
        """
        tabletop = Table((5, 5))
        test_robot = Robot(tabletop)
        test_robot.move()
        test_robot.rotation_left()
        test_robot.rotation_rigth()
        test_robot.report()

        benchmark = ['not in place']
        self.check_results(benchmark, test_robot)

    
    def test_altair_2(self) -> None:
        """ Case 2 coming from Altair
        check lace robot on a given position

        :return: None
        """
        tabletop = Table((5, 5))
        test_robot = Robot(tabletop)
        test_robot.place_robot(1, 2, 'NORTH')
        test_robot.report()
        benchmark = ['1,2,NORTH\n']

        self.check_results(benchmark, test_robot)


    def test_altair_3(self) -> None:
        """ Case 3 coming from Altair
        check robot is not placed in an invalid position

        :return: None
        """
        tabletop = Table((5, 5))
        test_robot = Robot(tabletop)
        test_robot.place_robot(9, 9, 'NORTH')
        test_robot.report()
        benchmark = ['not in place']
        self.check_results(benchmark, test_robot)

    def test_altair_4(self) -> None:
        """ Case 4 coming from Altair
        check robot rotates right

        :return: None
        """
        tabletop = Table((5, 5))
        test_robot = Robot(tabletop)
        test_robot.place_robot(1, 1, 'NORTH')
        test_robot.rotation_rigth()
        test_robot.report()
        test_robot.rotation_rigth()
        test_robot.report()
        test_robot.rotation_rigth()
        test_robot.report()
        test_robot.rotation_rigth()
        test_robot.report()
        benchmark = ['1,1,EAST\n',
                     '1,1,SOUTH\n',
                     '1,1,WEST\n',
                     '1,1,NORTH\n']
        self.check_results(benchmark, test_robot)


    def test_altair_5(self) -> None:
        """ Case 5 coming from Altair
        check robot rotates left

        :return: None
        """
        tabletop = Table((5, 5))
        test_robot = Robot(tabletop)
        test_robot.place_robot(1, 1, 'NORTH')
        test_robot.rotation_left()
        test_robot.report()
        test_robot.rotation_left()
        test_robot.report()
        test_robot.rotation_left()
        test_robot.report()
        test_robot.rotation_left()
        test_robot.report()
        benchmark = ['1,1,WEST\n',
                     '1,1,SOUTH\n',
                     '1,1,EAST\n',
                     '1,1,NORTH\n']
        self.check_results(benchmark, test_robot)

    def test_altair_6(self) -> None:
        """ Case 6 coming from Altair
        check robot is able to move in all four cardinal directions

        :return: None
        """

        tabletop = Table((5, 5))
        test_robot = Robot(tabletop)
        test_robot.place_robot(1, 1, 'NORTH')
        test_robot.move()
        test_robot.rotation_rigth()
        test_robot.report()
        test_robot.move()
        test_robot.rotation_rigth()
        test_robot.report()
        test_robot.move()
        test_robot.rotation_rigth()
        test_robot.report()
        test_robot.move()
        test_robot.rotation_rigth()
        test_robot.report()
        benchmark = ['1,2,EAST\n',
                     '2,2,SOUTH\n',
                     '2,1,WEST\n',
                     '1,1,NORTH\n']
        self.check_results(benchmark, test_robot)

    def test_altair_7(self) -> None:
        """ Case 7 coming from Altair
         check robot won't fall off the table

        :return: None
        """
        tabletop = Table((5, 5))
        test_robot = Robot(tabletop)
        test_robot.place_robot(4, 4, 'NORTH')
        test_robot.move()
        test_robot.move()
        test_robot.move()
        test_robot.move()
        test_robot.move()
        test_robot.report()
        benchmark = ['4,4,NORTH\n']
        self.check_results(benchmark, test_robot)

    def check_results(self, benchmark: str, robot: Robot) -> None:
        """Read the output file where results are stored and compare them
        against the desired output to test the clase Robot

        :param benchmark: expected result
        :param robot: object of robot with table size of 5x5
        :return: None
        """
        with open(robot.file) as f:
            lines = f.readlines()

        self.assertEqual(lines, benchmark, f'The result should be {benchmark}')

if __name__ == '__main__':
    unittest.main()