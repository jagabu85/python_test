from robot import Robot

ALLOWED_VALUES = ('FILE', 'CLI')


class CLI:
    """ Command-line interface used to interact with program
    """

    def __init__(self, tabletop) -> None:
        self.tabletop = tabletop

    def read_input_method(self) -> str:
        """ Read from prompt the order to select the desired input method

        :return order: Str read from the prompt, it represents the
        selected input method
        """
        self.print_spliting_line()
        print(f'Please select one of the 2 input method:\n'
              'FILE : All the commands will be readed from a file\n'
              'CLI  : The user will type the desired command in the console')
        order = self.wait_correct_answer(ALLOWED_VALUES)
        return order

    def wait_correct_answer(self, allowed_values: tuple) -> str:
        """ Reads an input from prompt until it exist in the list
        allowed_values

        :param [tuple] allowed_values: Tuple containing the available options
        :return value: Str read from the prompt, it represents the
        selected input method
        """
        value = input()
        while value not in allowed_values:
            self.print_supplied_order(value)
            self.print_allowed_orders(allowed_values)
            value = input()
        return value

    def print_initial_message(self, input_method: str, orders: tuple) -> None:
        """ Print the initial message to start the game. It indicates the
        select input method

        :param [str] input_method: Define how the orders will be introduced
        :param [list] orders: List of implemented orders, in order the player
        knows what he can write
        :return: None
        """
        print(f'You have choose to add the orders {input_method}.')
        print(f'The allowed commands for the robot are:'
              f' {orders}.')
        self.print_spliting_line()

    # -------------------------------------------------------------------------
    def read_init_place_order(self, robot: Robot) -> dict:
        """ Reads the orders introduced in the prompt and ignore them until a
        proper PLACE order is introduced with the correct arguments format

        :param [Robot] robot: More info robot.py
        :return: Dictionary containing the configuration for the first PLACE
        order
        :rtype: dict
        """
        self.print_first_step_place()
        self.print_place_format_info()
        self.print_spliting_line()
        line = input()
        while not self.check_place_order(line, robot):
            self.execute_report_order(line, robot)
            line = input()

        configuration = self.get_configuration_from_place_order(line)
        return configuration

    @staticmethod
    def execute_report_order(line: str, robot: Robot) -> None:
        """ Execute the report order

        :param [str] line: Line get from prompt while waiting place order
        :param robot: object of class Robot
        :return: None
        """
        if line == 'REPORT':
            robot.report()

    @staticmethod
    def get_configuration_from_place_order(order: str) -> dict:
        """ Convert the string with order place and the arguments to a
        dictionary containing the configuration parameters X, Y and orientation

        :param order: The string read from the prompt. I starts with PLACE
        and also have the 3 arguments separated by coma
        :return: Dictionary containing the arguments X, Y and orientation
        :rtype: dict
        """
        order_arguments = order.split(sep=" ")[1].split(sep=",")

        configuration = {'x': int(order_arguments[0]),
                         'y': int(order_arguments[1]),
                         'orientation': order_arguments[2]
                         }
        return configuration

    def check_place_order(self, order: str, robot: Robot) -> bool:
        """Check if the order read from the prompt starting with PLACE has all
        the arguments in the proper configuration

        :param [str] order: The string read from the prompt.
        :param [Robot] robot: More info robot.py
        :return: Return true if all the check are successful otherwise false
        :rtype: bool
        """

        if (self.check_if_order_is_place(order) and
                self.check_if_one_space(order) and
                self.check_if_separated_by_points(order) and
                self.check_passing_3_arguments(order) and
                self.check_valid_orientation(order, robot) and
                self.check_inside_tabletop(order, robot)):
            print('Inside')
            return True
        else:
            return False

    def check_inside_tabletop(self, order: str, robot: Robot) -> bool:
        """ Check if the position defined by x and y are inside the limits
        of the tabletop

        :param [str] order: The string read from the prompt.
        :param [Robot] robot: More info robot.py
        :return: Return true if order is a valid orientation, false otherwise
        :rtype: bool
        """

        configuration = self.get_configuration_from_place_order(order)
        if (self.tabletop.is_in_the_domain(configuration['x'],
                                           self.tabletop._x_domain) and
            self.tabletop.is_in_the_domain(configuration['y'],
                                              self.tabletop._y_domain)):
            return True
        else:
            self.print_supplied_order(order)
            print(f'Make sure to pass a position wich is inside the '
                  f'tabletop, the table top size is: '
                  f'{self.tabletop._x_domain[1]} x '
                  f'{self.tabletop._y_domain[1]}')
            return False

    def check_valid_orientation(self, order: str, robot: Robot) -> bool:
        """ Check the order contains one of the valid orientations defined
        in the class Robot

        :param [str] order: The string read from the prompt.
        :param [Robot] robot: More info robot.py
        :return: Return true if order is a valid orientation, false otherwise
        :rtype: bool
        """
        if order.split(' ')[1].split(',')[2] in robot.possible_directions:
            return True
        else:
            valid_orientation = False
            self.print_supplied_order(order)
            print(f'Make sure to pass one of the valid orientation: '
                  f'{robot.possible_directions}')
        return valid_orientation

    def check_if_order_is_place(self, order: str) -> bool:
        """Check if the order read from the prompt starts as PLACE.

        :param order: The string read from the prompt.
        :return: Return true if all the check is successful otherwise false
        :rtype: bool
        """
        if order[:5] == 'PLACE':
            return True
        else:
            self.print_supplied_order(order)
            self.print_first_step_place()
            return False

    def check_if_one_space(self, order: str) -> bool:
        """Check if the order read from the prompt starting with PLACE has a
        space between the order and the arguments.

        :param order: The string read from the prompt.
        :return: Return true if all the check is successful otherwise false
        :rtype: bool
        """
        if order.count(' ') == 1:
            return True
        else:
            self.print_supplied_order(order)
            print('Make sure to pass all arguments. Leave one space between '
                  'the order and the first argument')
            return False

    def check_if_separated_by_points(self, order: str) -> bool:
        """Check if the order read from the prompt starting with PLACE has
        the arguments separated by points.

        :param order: The string read from the prompt.
        :return: Return true if all the check is successful otherwise false
        :rtype: bool
        """
        if '.' in order:
            self.print_supplied_order(order)
            print('Make sure you split the arguments with coma instead of dot')
            return False
        else:
            return True

    def check_passing_3_arguments(self, order: str) -> bool:
        """Check if the order read from the prompt starting with PLACE has
        3 arguments

        :param order: The string read from the prompt.
        :return: Return true if all the check is successful otherwise false
        :rtype: bool
        """
        if order.count(',') == 2:
            return True
        else:
            self.print_supplied_order(order)
            print('Make sure you are passing 3 arguments')
            return False

    @staticmethod
    def print_first_step_place() -> None:
        print('The first step to play is to place the robot. Use the PLACE '
              'order')

    @staticmethod
    def print_place_format_info() -> None:
        print('The format of the order PLACE is as follow: PLACE X, Y, F \n'
              'where X, Y are the pair of coordinates and F is '
              'the initial direction the robot is facing.')

    # -------------------------------------------------------------------------
    def read_order(self, allowed_orders: tuple) -> str:
        """ Read from the prompt the orders and check if they are compatible
        with the available/implemented orders

        :param [tuple] allowed_orders: Tuple containing the available orders
        for the robot
        :return:
        """
        print(f'Enter one of the following orders:\n{allowed_orders}')
        line = input()
        if line.split(sep=" ")[0] in allowed_orders or line == 'EXIT':
            return line
        else:
            self.print_supplied_order(line)
            self.print_allowed_orders(allowed_orders)
            return ''

    # -------------------------------------------------------------------------
    @staticmethod
    def print_supplied_order(order: str) -> None:
        print(f'The order supplied was "{order}".')

    @staticmethod
    def print_allowed_orders(allowed_values: tuple) -> None:
        print(f'The only allowed orders are : {allowed_values}')

    @staticmethod
    def print_spliting_line() -> None:
        print('-------------------------------------------'
              '-------------------------------------------')

    @staticmethod
    def print_ending_message() -> None:
        print(' The game has ended by the user')
