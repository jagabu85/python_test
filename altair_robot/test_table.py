import unittest
import numpy as np

from table import Table

class TestRobot(unittest.TestCase):

    def test_is_in_tabletop(self) -> None:
        """ Check lower outside, in adn upper ouside of the table in both
        directions

        :return: None
        """
        tabletop = Table((5,5))

        tests = [(-1,1), (5,1), (1,-1), (1,5), (2,2)]
        expected_results = [False, False, False, False, True]

        for i, value in enumerate(tests):
            position = tabletop.is_in_tabletop(np.array(value))
            self.assertEqual(position, expected_results[i],
                         f'The result should be {expected_results[i]}')

    def test_is_in_the_domain(self) -> None:
        """ Check lower outside, in adn upper ouside of the domain

        :return: None
        """
        tabletop = Table((5, 5))
        tests = [-1, 0, 1, 4, 5]
        expected_results = [False, True, True, True, False]

        for i, value in enumerate(tests):
            self.assertEqual(
                tabletop.is_in_the_domain(value, tabletop._x_domain),
                expected_results[i],
                f'The result should be {expected_results[i]}')

        for i, value in enumerate(tests):
            self.assertEqual(
                tabletop.is_in_the_domain(value, tabletop._y_domain),
                expected_results[i],
                f'The result should be {expected_results[i]}')

if __name__ == '__main__':
    unittest.main()